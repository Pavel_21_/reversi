package com.example;


import java.util.Random;
import java.util.Scanner;

public class MyClass {
    /* public static int[][] move;

    public static void down(int player, int y, int x) {
        for (int j = y + 1; j < 10; j++) {
            if (move[j][x] == 0) {
                break;
            } else if (move[j][x] != player) {
                j = j + 1;
            }
            if (j > y + 1 && move[j][x] == player) {
                for (int tempY = y; tempY < j; tempY++) {
                    move[tempY][x] = player;
                }
            }
        }
    }// working

    public static void up(int player, int y, int x) {
        for (int j = y - 1; j >= 0; j--) {
            if (move[j][x] == 0) {
                break;
            } else if (move[j][x] != player) {
                j = j - 1;
            }
            if ((j < y - 1) && (move[j][x] == player)) {
                for (int tempY = y; tempY > j; tempY--) {
                    move[tempY][x] = player;
                }
            }
        }
    }  // working

    public static void right(int player, int y, int x) {
        for (int i = x + 1; i < 10; i++) {
            if (move[y][i] == 0) {
                break;
            } else if (move[y][i] != player) {
                i = i + 1;
            }
            if ((i > x + 1) && (move[y][i] == player)) {
                for (int tempX = x; tempX < i; tempX++) {
                    move[y][tempX] = player;
                }
            }
        }
    } // working

    public static void left(int player, int y, int x) {
        for (int i = x - 1; i >= 0; i--) {
            if (move[y][i] == 0) {
                break;
            } else if (move[y][i] != player) {
                i = i - 1;
            }
            if (i < x - 1 && move[y][i] == player) {
                for (int tempX = x; tempX > i; tempX--) {
                    move[y][tempX] = player;
                }
            }
        }
    } // working


    public static void downLeft(int player, int y, int x) {
        int i = x - 1;
        int j = y + 1;
        if (move[j][i] == 0 || move[j][i] == player) {
            return;
        }
        while (j < 10 && i > 1) {
            i = i - 1;
            j = j + 1;
            if (move[j][i] == 0) {
                return;
            }

            if (move[j][i] != player && move[j + 1][i - 1] == player) {
                int tempX = i;
                int tempY = j;
                while (tempY >= y && tempX <= x) {
                    move[tempY][tempX] = player;
                    tempX += 1;
                    tempY -= 1;

                }
            }
        }
    }

    public static void upLeft(int player, int y, int x) {
        int i = x - 1;
        int j = y - 1;
        if (move[j][i] == 0 || move[j][i] == player) {
            return;
        }
        while (i > 1 && j > 1) {
            i = i - 1;
            j = j - 1;
            if (move[j][i] == 0) {
                return;
            }
            if (move[j][i] != player && move[j - 1][i - 1] == player) {
                while (i <= x && j <= y) {
                    move[j][i] = player;
                    i++;
                    j++;

                }
            }
        }
    }

    public static void upRight(int player, int y, int x) {
        int i = x + 1;
        int j = y - 1;
        if (move[j][i] == 0 || move[j][i] == player) {
            return;
        }
        while (j > 1 && i < 10) {
            j = j - 1;
            i = i + 1;
            if (move[j][i] == 0) {
                return;
            }
            if (move[j][i] != player && move[j - 1][i + 1] == player) {
                int tempX = i;
                int tempY = j;
                while (tempY <= y && tempX >= x) {
                    move[tempY][tempX] = player;
                    tempX -= 1;
                    tempY += 1;
                }
            }
        }
    } //

    public static void downRight(int player, int y, int x) {//
        int i = y;
        int j = x;
        if (move[j + 1][i + 1] == 0 || move[j + 1][i + 1] == player) {
            return;
        }
        while (i < 10 && j < 10) {
            i = i + 1;
            j = j + 1;
            if (move[j][i] == 0) {
                return;
            }
            if (move[j][i] != player && move[j + 1][i + 1] == player) {
                while (move[j][i] != player) {

                    move[j - 1][i - 1] = player;
                    move[j][i] = player;

                }
            }
        }
    } //

    public static boolean canPlace(int player, int y, int x) {
        if (move[y][x] != 0)
            return false;
        if (move[y + 1][x] != 0 || move[y + 1][x + 1] != 0 || move[y][x + 1] != 0
                || move[y - 1][x + 1] != 0 || move[y - 1][x - 1] != 0
                || move[y][x - 1] != 0 || move[y - 1][x] != 0 || move[y + 1][x - 1] != 0)
            return true;

        return false;

    }

    public static boolean placeAt(int player, int y, int x) {
        if (x < 1 || y < 1 || x > 9 || y > 9 || !canPlace(player, y, x)) {
            System.out.println("banana");
            return false;
        }
        move[y][x] = player;
        return true;
    }

    public static void printField() {
        System.out.println("\t  | 1 2 3 4 5 6 7 8");
        for (int i = 1; i < 9; i++) {
            System.out.format("\t %d| ", i);
            for (int j = 1; j < 9; j++)
                System.out.print(move[i][j] + " ");
            System.out.print("\n");
        }
    }

    public static void main(String args[]) {
        move = new int[10][10];
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                move[i][j] = 0;
        move[4][5] = 1;
        move[4][4] = 2;
        move[5][4] = 1;
        move[5][5] = 2;
        printField();
        int currentPlayer = 1, tmpX, tmpY;
        while (true) {
            tmpY = sc.nextInt();
            tmpX = sc.nextInt();

            if (placeAt(currentPlayer, tmpY, tmpX)) {
                right(currentPlayer, tmpY, tmpX);
                up(currentPlayer, tmpY, tmpX);
                left(currentPlayer, tmpY, tmpX);
                down(currentPlayer, tmpY, tmpX);
                downLeft(currentPlayer, tmpY, tmpX);
                upLeft(currentPlayer, tmpY, tmpX);



                /*

                upRight(currentPlayer, tmpY, tmpX);
                downRight(currentPlayer, tmpY, tmpX);






                currentPlayer = (currentPlayer == 1 ? 2 : 1);
                printField();

            } else {
                System.out.println("You can not make this move!");
                printField();
            }
        }
    }*/
}
